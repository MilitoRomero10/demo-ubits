package com.demo.backendubits.repositories;

import com.demo.backendubits.models.Functionality;
import org.springframework.data.repository.CrudRepository;

public interface FunctionalityRepository extends CrudRepository<Functionality, Long> {

}
