package com.demo.backendubits.repositories;

import com.demo.backendubits.models.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
