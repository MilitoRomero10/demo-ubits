package com.demo.backendubits.repositories;

import com.demo.backendubits.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
