package com.demo.backendubits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendUbitsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendUbitsApplication.class, args);
    }

}
